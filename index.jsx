import React from 'react';
import ReactDom from 'react-dom';

const HelloComponent = () => {
    return <h1>You're Smart People</h1>;
}

ReactDOM.render(<HelloComponent />, document.getElementById('root'));